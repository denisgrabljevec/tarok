angular.module('myApp', ['ui.router', 'ngSanitize', 'ui.select'])

.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'domov.html',
            /*resolve: {
                userInfoData: function (igralciService, $http) {
                    return $http.get('podatki.json').then(function(data){
                        igralciService.igralci = data.data;
                        return data.data
                    });
                }
            }*/
        });
        
})

.service('igralciService', function($rootScope, $http){
    var service = this;
    
    service.igralci = {"igralci": [
        {
            "id": 1,
            "ime": "Matija*"
        },
        {
            "id": 2,
            "ime": "Denis"
        },
        {
            "id": 3,
            "ime": "Jure"
        },
        {
            "id": 4,
            "ime": "Luka"
        }
    ]};
    
    service.igre = [];
    
})

.controller('myController', function($rootScope, $scope, igralciService){

    $scope.igralci = igralciService.igralci;
    
    $scope.igra = {};
    
    $scope.igre = igralciService.igre;
    
    var retrievedObject = JSON.parse(localStorage.getItem('rez'));
    
    if(retrievedObject != null){
        
        $scope.igre = retrievedObject;
    }
    
    
    
    
    
    
    
    
    
    var final_transcript = '';
    var recognizing = false;
    var ignore_onend;
    var start_timestamp;
    
    
    if (!('webkitSpeechRecognition' in window)){
        upgrade();
    }
    else {
        var recognition = new webkitSpeechRecognition();
        recognition.continuous = true;
        recognition.interimResults = true;
        
        recognition.lang = "sl-SI";
        
        recognition.onstart = function() {
            recognizing = true;
        };
        
        recognition.onresult = function(event) {
            var interim_transcript = '';
            for (var i = event.resultIndex; i < event.results.length; ++i) {
                if (event.results[i].isFinal) {
                    final_transcript += event.results[i][0].transcript;
                    //prepoznaj(final_transcript);
                } else {
                    interim_transcript += event.results[i][0].transcript;
                }
            }
            prepoznaj(interim_transcript);
            
        };
        
        recognition.onend = function() {
            recognizing = false;
            if (ignore_onend) {
                return;
            }
        };
    }
    
    $scope.zacniGovor = function(event){
        if (recognizing) {
            recognition.stop();
            return;
        }
        
        final_transcript = '';
        recognition.start();
        
        ignore_onend = false;
        
    }
    
    function prepoznaj(transcript){
        if(transcript == "") return;
        var res = transcript.split(" ");
        for(var i = 0; i < res.length; i++){
            if(res[i].length == 0){
                res.splice(0, 1);
                continue;
            }
            res[i] = res[i].replace(" ","");
            console.log("-"+ res[i]+"-");
            
        }
        console.log();
        
        if(res[0] == "dodaj" || res[0] == "vpiši" || res[0] == "potrdi" || res[0] == "sprejmi" || res[0] == "ok" || res[0] == "okej"){
            transcript = '';
            $scope.dodajIgro();
            final_transcript = '';
            $scope.$apply();
            return;
        }
        
        if(res[0] == "izbriši" || res[0] == 'zbriši' || res[0] == 'briši'){
            $scope.izbrisiIgro();
            final_transcript = '';
            $scope.$apply();
            return;
        }
        
        if(res[0] == "matija" || res[0] == "hribi"){
            if(res[1] == "minus" || res[1] == "linus"){
                $scope.igra['1'] = '-'+res[2];
            }
            else{
                $scope.igra['1'] = res[1];
            }
        }
        else if(res[0] == "jure"){
            if(res[1] == "minus" || res[1] == "linus"){
                $scope.igra['3'] = '-'+res[2];
            }
            else{
                $scope.igra['3'] = res[1];
            }
        }
        else if(res[0] == "denis" || res[0] == "tenis"){
            if(res[1] == "minus" || res[1] == "linus"){
                $scope.igra['2'] = '-'+res[2];
            }
            else{
                $scope.igra['2'] = res[1];
            }
        }
        else if(res[0] == "luka" || res[0] == "rojc" || res[0] == "rojec"){
            if(res[1] == "minus" || res[1] == "linus"){
                $scope.igra['4'] = '-'+res[2];
            }
            else{
                $scope.igra['4'] = res[1];
            }
        }
        $scope.$apply();
        final_transcript = '';
    }
    
    
    $scope.dodajEdino = function(id){
        var tocke = parseInt($scope.igra[id]);
        
        var tmp = (-1)*(tocke / 3);
        
        var index = $scope.igre.length;
        var zadnjaIgra = (index == 0) ? 0 : $scope.igre[index-1];
        
        if(index == 0){
            if(id == 1){
                dodaj(tocke, tmp, tmp, tmp);
            }
            else if(id == 2){
                dodaj(tmp, tocke, tmp, tmp);
            }
            else if(id == 3){
                dodaj(tmp, tmp, tocke, tmp);
            }
            else if(id == 4){
                dodaj(tmp, tmp, tmp, tocke);
            }
        }
        else{
            if(id == 1){
                dodaj((zadnjaIgra['1']+tocke), (zadnjaIgra['2']+tmp), (zadnjaIgra['3']+tmp), (zadnjaIgra['4']+tmp));
            }
            else if(id == 2){
                dodaj((zadnjaIgra['1']+tmp), (zadnjaIgra['2']+tocke), (zadnjaIgra['3']+tmp), (zadnjaIgra['4']+tmp));
            }
            else if(id == 3){
                dodaj((zadnjaIgra['1']+tmp), (zadnjaIgra['2']+tmp), (zadnjaIgra['3']+tocke), (zadnjaIgra['4']+tmp));
            }
            else if(id == 4){
                dodaj((zadnjaIgra['1']+tmp), (zadnjaIgra['2']+tmp), (zadnjaIgra['3']+tmp), (zadnjaIgra['4']+tocke));
            }
        }
                
        $scope.igra = {};
        localStorage.setItem('rez', JSON.stringify($scope.igre));
        
    };
    
    function dodaj(prvi, drugi, tretji, cetrti){
        $scope.igre.push({
            1: prvi,
            2: drugi,
            3: tretji,
            4: cetrti
        });
    }
    
    $scope.dodajIgro = function(){
        
        var index = $scope.igre.length;
        var zadnjaIgra = (index == 0) ? 0 : $scope.igre[index-1];
        
        if(index != 0 && ((zadnjaIgra['1'] + zadnjaIgra['2'] + zadnjaIgra['3'] + zadnjaIgra['4']) == 0)){
            var prvi = zadnjaIgra['1'] + parseInt($scope.igra['1']);
            var drugi = zadnjaIgra['2'] + parseInt($scope.igra['2']);
            var tretji = zadnjaIgra['3'] + parseInt($scope.igra['3']);
            var cetrti = zadnjaIgra['4'] + parseInt($scope.igra['4']);
            
            if((prvi + drugi + tretji + cetrti) == 0){
                $scope.igre.push({
                    1: prvi,
                    2: drugi,
                    3: tretji,
                    4: cetrti
                });
                
                $scope.igra = {};
                localStorage.setItem('rez', JSON.stringify($scope.igre));
                
            }
            else{
                alert("Matija, nauči se seštevati!");
            }
        }
        else if(index != 0 && zadnjaIgra['1'] + zadnjaIgra['2'] + zadnjaIgra['3'] + zadnjaIgra['4'] != 0){
            alert("Matija, nauči se seštevati!");
        }
        else{
            if((parseInt($scope.igra['1']) + parseInt($scope.igra['2']) + parseInt($scope.igra['3']) + parseInt($scope.igra['4'])) == 0){
                $scope.igre.push({
                    1: parseInt($scope.igra['1']),
                    2: parseInt($scope.igra['2']),
                    3: parseInt($scope.igra['3']),
                    4: parseInt($scope.igra['4'])
                });
                
                $scope.igra = {};
                localStorage.setItem('rez', JSON.stringify($scope.igre));
            }
            else {
                alert("Matija, nauči se seštevati!");
            }
        }
    };
    
    $scope.izbrisiIgro = function(){
        var r = confirm("Boš zagotovo brisal Matija?");
        if(r){
            var retrievedObject1 = JSON.parse(localStorage.getItem('rez'));
        
            $scope.igre.pop();
        
            localStorage.setItem('rez', JSON.stringify($scope.igre));
        }
    };
})